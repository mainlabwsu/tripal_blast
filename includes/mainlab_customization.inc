<?php

/**
 * Submit a job to run drush command and specified tripal_job to the mainlab cluster
 */
function submit_to_mainlab_cluster ($job_id) {
  $current_dir = getcwd();
  $admin = db_select('users', 'u')->fields('u', array('name'))->condition('uid', 1)->execute()->fetchField();
  $nfs_dir = $current_dir;
  $mod_dir = drupal_get_path('module', 'blast_ui');
  $host = gethostname();
  if (file_exists($mod_dir . '/includes/nfs_dir.txt')) {
    $nfs_dir = trim(file_get_contents($mod_dir . '/includes/nfs_dir.txt'));
    $nfs_dir = str_replace('$HOSTNAME', $host, $nfs_dir);
  }
  else {    
    if ($host == 'hort-wp') {
      $nfs_dir = str_replace(array('/local/sites'), array('/main/sites/hort-wp'), $current_dir);
    }
    if ($host == 'hort-wp2') {
      $nfs_dir = str_replace(array('/local/sites'), array('/main/sites/hort-wp2'), $current_dir);
    }
    else if ($host == 'hort-wd') {
      $nfs_dir = str_replace(array('/local/sites'), array('/main/sites/hort-wd'), $current_dir);
    }
    else {
      $nfs_dir = str_replace(array('/local/sites/dev', '/local/sites/www'), array('/main/sites/dev/dev', '/main/sites/www/www'), $current_dir);
    }
  }
  $cmd = "ml drush; cd $nfs_dir; drush trp-run-jobs --username=$admin --parallel=1 --job_id=$job_id";
  $tmp_dir = function_exists('file_directory_temp') ? file_directory_temp() : '/tmp';
  $script = $tmp_dir . "/blast_job.$job_id.sh";
  $handle = fopen($script, 'w');
  fwrite($handle, "#!/bin/bash\n");
/*  fwrite($handle, "#\$ -N tripal.$job_id\n");
  fwrite($handle, "#\$ -o $tmp_dir/qsub.$job_id.out\n");
  fwrite($handle, "#\$ -e $tmp_dir/qsub.$job_id.err\n"); */
  fwrite($handle, "#SBATCH --partition=web\n");
  fwrite($handle, "#SBATCH --account=tripal\n");
  fwrite($handle, "#SBATCH --output=$tmp_dir/squeue.$job_id.out\n");
  fwrite($handle, "#SBATCH --error $tmp_dir/squeue.$job_id.err\n");
  fwrite($handle, "#SBATCH --job-name tripal.$job_id\n");
  fwrite($handle, "#SBATCH --get-user-env\n");
  fwrite($handle, "#SBATCH --nodes=1\n");
  fwrite($handle, "#SBATCH --ntasks=1\n");
  fwrite($handle, "#SBATCH --chdir=$nfs_dir\n");
  fwrite($handle, "#SBATCH --cpus-per-task=1\n");
  fwrite($handle, "#SBATCH --time=24:00:00\n");
  fwrite($handle, $cmd);
  fclose($handle);

  exec("/etc/slurm/bin/sbatch $script", $stdout, $return);

  /* exec("export SGE_ROOT=/ge;/ge/bin/linux-x64/qsub $script", $stdout, $return); */
  /*exec("export SGE_ROOT=/ge;sleep 5;/ge/bin/linux-x64/qsub $script", $stdout, $return);*/
}

/**
 * Group and reorganize Blast UI databases according to curator's preference
 *
 * @param unknown $options
 * @return unknown
 */
function organize_blast_ui_dbs ($options) {
  $current_dir = getcwd();
  if (preg_match('/rosaceae/', $current_dir)) {
    $options = organize_gdr_blast_dbs($options);
  }
  else if (preg_match('/citrusgenomedb/', $current_dir)) {
    $options = organize_citrus_blast_dbs($options);
  }
  else if (preg_match('/cottongen/', $current_dir)) {
    $options = organize_cottongen_blast_dbs($options);
  }
  else if (preg_match('/pulsedb/', $current_dir)) {
    $options = organize_legume_blast_dbs($options);
  }
  return $options;
}

/**
 * GDR specific blast db grouping
 *
 * @param unknown $options
 * @return unknown
 */
function organize_gdr_blast_dbs ($options) {
  $new_opts =
  array(
    0 => 'SELECT a Dataset',
    'Cerasus x kanzakura genome' => array(),
    'Cerasus speciosa genome' => array(),
    'Crataegus pinnatifida genome' => array(),
    'Eriobotrya japonica genome' => array(),
    'Fragaria ananassa genome' => array(),
    'Fragaria chiloensis genome' => array(),
    'Fragaria daltoniana genome' => array(),
    'Fragaria iinumae genome' => array(),
    'Fragaria mandschurica genome' => array(),
    'Fragaria moupinensis genome' => array(),
    'Fragaria nilgerrensis genome' => array(),
    'Fragaria nipponica genome' => array(),
    'Fragaria nubicola genome' => array(),
    'Fragaria orientalis genome' => array(),
    'Fragaria pentaphylla genome' => array(),
    'Fragaria vesca genome' => array(),
    'Fragaria virginiana genome' => array(),
    'Fragaria viridis genome' => array(),
    'Fragaria x ananassa genome' => array(),
    'Gillenia trifoliata genome' => array(),
    'Malus angustifolia genome' => array(),
    'Malus baccata genome' => array(),
    'Malus coronaria genome' => array(),
    'Malus fusca genome' => array(),
    'Malus ioensis genome' => array(),
    'Malus prunifolia genome' => array(),
    'Malus sieversii genome' => array(),
    'Malus sylvestris genome' => array(),
    'Malus x domestica genome' => array(),
    'Potentilla anserina genome' => array(),
    'Potentilla micrantha genome' => array(),
    'Prunus armeniaca genome' => array(),
    'Prunus avium genome' => array(),
    'Prunus campanulata genome' => array(),
    'Prunus cerasus genome' => array(),
    'Prunus davidiana genome' => array(),
    'Prunus domestica genome' => array(),
    'Prunus dulcis genome' => array(),
    'Prunus ferganensis genome' => array(),
    'Prunus fruticosa genome' => array(),
    'Prunus humilis genome' => array(),
    'Prunus kansuensis genome' => array(),
    'Prunus mandshurica genome' => array(),
    'Prunus mira genome' => array(),
    'Prunus mongolica genome' => array(),
    'Prunus mume genome' => array(),
    'Prunus persica genome' => array(),
    'Prunus salicina genome' => array(),
    'Prunus sanyueli genome' => array(),
    'Prunus sibirica genome' => array(),
    'Prunus yedoensis genome' => array(),
    'Prunus zhengheensis genome' => array(),
    'Pyrus betulifolia genome' => array(),
    'Pyrus bretschneideri genome' => array(),
    'Pyrus communis genome' => array(),
    'Pyrus pyrifolia genome' => array(),
    'Pyrus ussuriensis x communis genome' => array(),
    'Rosa chinensis genome' => array(),
    'Rosa multiflora genome' => array(),
    'Rosa rugosa genome' => array(),
    'Rosa wichuraiana genome' => array(),
    'Rubus argutus genome' => array(),
    'Rubus chingii genome' => array(),
    'Rubus idaeus genome' => array(),
    'Rubus occidentalis genome' => array(),
    'Rubus ulmifolis genome' => array(),
    'Rubus ulmifolius genome' => array(),
    'GDR Unigenes v5' => array(),
    'RefTrans' => array(),
    'Other' => array()
  );
  foreach ($options AS $k => $v) {
    if (preg_match('/GDR .*Unigene v5/i', $v)) {
      $new_opts['GDR Unigenes v5'][$k] = $v;
    }
    else if (preg_match('/RefTrans/i', $v)) {
      $new_opts['RefTrans'][$k] = $v;
    }
    else if (preg_match('/Cerasus x kanzakura/i', $v)) {
      $new_opts['Cerasus x kanzakura genome'][$k] = $v;
    }
    else if (preg_match('/Cerasus speciosa/i', $v)) {
      $new_opts['Cerasus speciosa genome'][$k] = $v;
    }
    else if (preg_match('/Crataegus pinnatifida/i', $v)) {
      $new_opts['Crataegus pinnatifida genome'][$k] = $v;
    }
    else if (preg_match('/Eriobotrya japonica/i', $v)) {
      $new_opts['Eriobotrya japonica genome'][$k] = $v;
    }
    else if (preg_match('/Black raspberry genome/i', $v) || preg_match('/Rubus occidentalis/i', $v)) {
      $new_opts['Rubus occidentalis genome'][$k] = $v;
    }
    else if (preg_match('/Peach|Prunus persica/i', $v)) {
      $new_opts['Prunus persica genome'][$k] = $v;
    }
    else if (preg_match('/Pyrus pyrifolia/i', $v)) {
        $new_opts['Pyrus pyrifolia genome'][$k] = $v;
    }
    else if (preg_match('/Sweet cherry genome|prunus avium/i', $v)) {
      $new_opts['Prunus avium genome'][$k] = $v;
    }
    else if (preg_match('/prunus campanulata/i', $v)) {
      $new_opts['Prunus campanulata genome'][$k] = $v;
    }
    else if (preg_match('/prunus cerasus/i', $v)) {
      $new_opts['Prunus cerasus genome'][$k] = $v;
    }
    else if (preg_match('/Prunus armeniaca/i', $v)) {
        $new_opts['Prunus armeniaca genome'][$k] = $v;
    }
    else if (preg_match('/Prunus yedoensis/i', $v) || preg_match('/Cerasus x yedoensis/i', $v)) {
      $new_opts['Prunus yedoensis genome'][$k] = $v;
    }
    else if (preg_match('/Prunus zhengheensis/i', $v)) {
      $new_opts['Prunus zhengheensis genome'][$k] = $v;
    }
    else if (preg_match('/Prunus dulcis/i', $v)) {
        $new_opts['Prunus dulcis genome'][$k] = $v;
    }
    else if (preg_match('/Prunus mandshurica/i', $v)) {
      $new_opts['Prunus mandshurica genome'][$k] = $v;
    }
    else if (preg_match('/Prunus mira /i', $v)) {
        $new_opts['Prunus mira genome'][$k] = $v;
    }
    else if (preg_match('/Prunus mongolica /i', $v)) {
      $new_opts['Prunus mongolica genome'][$k] = $v;
    }
    else if (preg_match('/Prunus mume /i', $v)) {
      $new_opts['Prunus mume genome'][$k] = $v;
    }
    else if (preg_match('/Prunus humilis/i', $v)) {
        $new_opts['Prunus humilis genome'][$k] = $v;
    }
    else if (preg_match('/Prunus kansuensis/i', $v)) {
      $new_opts['Prunus kansuensis genome'][$k] = $v;
    }
    else if (preg_match('/Prunus ferganensis/i', $v)) {
        $new_opts['Prunus ferganensis genome'][$k] = $v;
    }
    else if (preg_match('/Prunus fruticosa/i', $v)) {
      $new_opts['Prunus fruticosa genome'][$k] = $v;
    }
    else if (preg_match('/Prunus davidiana/i', $v)) {
        $new_opts['Prunus davidiana genome'][$k] = $v;
    }
    else if (preg_match('/Prunus domestica/i', $v)) {
        $new_opts['Prunus domestica genome'][$k] = $v;
    }
    else if (preg_match('/Prunus salicina/i', $v)) {
        $new_opts['Prunus salicina genome'][$k] = $v;
    }
    else if (preg_match('/Prunus sanyueli/i', $v)) {
      $new_opts['Prunus sanyueli genome'][$k] = $v;
    }
    else if (preg_match('/Prunus sibirica/i', $v)) {
      $new_opts['Prunus sibirica genome'][$k] = $v;
    }
    else if (preg_match('/Malus x domestica/i', $v) || preg_match('/Apple genome/i', $v)) {
      $new_opts['Malus x domestica genome'][$k] = $v;
    }
    else if (preg_match('/Malus prunifolia/i', $v)) {
      $new_opts['Malus prunifolia genome'][$k] = $v;
    }
    else if (preg_match('/Malus angustifolia/i', $v)) {
      $new_opts['Malus angustifolia genome'][$k] = $v;
    }
    else if (preg_match('/Malus baccata/i', $v)) {
        $new_opts['Malus baccata genome'][$k] = $v;
    }
    else if (preg_match('/Malus coronaria/i', $v)) {
      $new_opts['Malus coronaria genome'][$k] = $v;
    }
    else if (preg_match('/Malus fusca/i', $v)) {
      $new_opts['Malus fusca genome'][$k] = $v;
    }
    else if (preg_match('/Malus ioensis/i', $v)) {
      $new_opts['Malus ioensis genome'][$k] = $v;
    }
    else if (preg_match('/Malus sieversii/i', $v)) {
      $new_opts['Malus sieversii genome'][$k] = $v;
    }
    else if (preg_match('/Malus sylvestris/i', $v)) {
      $new_opts['Malus sylvestris genome'][$k] = $v;
    }
    else if (preg_match('/Strawberry genome/i', $v) || preg_match('/Fragaria.vesca/i', $v)) {
      $new_opts['Fragaria vesca genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria ananassa/i', $v)) {
      $new_opts['Fragaria ananassa genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria chiloensis /i', $v)) {
      $new_opts['Fragaria chiloensis genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria daltoniana /i', $v)) {
      $new_opts['Fragaria daltoniana genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria mandschurica /i', $v)) {
      $new_opts['Fragaria mandschurica genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria moupinensis /i', $v)) {
      $new_opts['Fragaria moupinensis genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria pentaphylla /i', $v)) {
      $new_opts['Fragaria pentaphylla genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria x ananassa /i', $v)) {
      $new_opts['Fragaria x ananassa genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria iinumae /i', $v)) {
      $new_opts['Fragaria iinumae genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria nilgerrensis/i', $v)) {
        $new_opts['Fragaria nilgerrensis genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria nipponica/i', $v) || preg_match('/Liberibacter/', $v)) {
      $new_opts['Fragaria nipponica genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria nubicola/i', $v)) {
      $new_opts['Fragaria nubicola genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria orientalis/i', $v)) {
      $new_opts['Fragaria orientalis genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria virginiana/i', $v)) {
      $new_opts['Fragaria virginiana genome'][$k] = $v;
    }
    else if (preg_match('/Fragaria viridis/i', $v)) {
        $new_opts['Fragaria viridis genome'][$k] = $v;
    }
    else if (preg_match('/Gillenia trifoliata/i', $v)) {
      $new_opts['Gillenia trifoliata genome'][$k] = $v;
    }
    else if (preg_match('/Potentilla anserina/i', $v)) {
      $new_opts['Potentilla anserina genome'][$k] = $v;
    }
    else if (preg_match('/Potentilla micrantha/i', $v)) {
      $new_opts['Potentilla micrantha genome'][$k] = $v;
    }
    else if (preg_match('/Pear .*genome/i', $v) || preg_match('/Pyrus communis/i', $v)) {
        $new_opts['Pyrus communis genome'][$k] = $v;
    }
    else if (preg_match('/Pyrus bretschneideri/i', $v)) {
        $new_opts['Pyrus bretschneideri genome'][$k] = $v;
    }
    else if (preg_match('/Pyrus betulifolia/i', $v)) {
        $new_opts['Pyrus betulifolia genome'][$k] = $v;
    }
    else if (preg_match('/Pyrus ussuriensis x communis/i', $v)) {
        $new_opts['Pyrus ussuriensis x communis genome'][$k] = $v;
    }
    else if (preg_match('/Rubus argutus/i', $v)) {
        $new_opts['Rubus argutus genome'][$k] = $v;
    }
    else if (preg_match('/Rubus idaeus/i', $v)) {
      $new_opts['Rubus idaeus genome'][$k] = $v;
    }
    else if (preg_match('/Rubus chingii/i', $v)) {
      $new_opts['Rubus chingii genome'][$k] = $v;
    }
    else if (preg_match('/Rubus ulmifolis/i', $v)) {
        $new_opts['Rubus ulmifolius genome'][$k] = $v;
    }
    else if (preg_match('/Rubus ulmifolius/i', $v)) {
      $new_opts['Rubus ulmifolius genome'][$k] = $v;
    }
    else if (preg_match('/Rosa chinensis/i', $v) || preg_match('/rchinensis/i', $v)) {
      $new_opts['Rosa chinensis genome'][$k] = $v;
    }
    else if (preg_match('/Rosa multiflora/i', $v)) {
      $new_opts['Rosa multiflora genome'][$k] = $v;
    }
    else if (preg_match('/Rosa rugosa /i', $v)) {
      $new_opts['Rosa rugosa genome'][$k] = $v;
    }
    else if (preg_match('/Rosa wichuraiana/i', $v)) {
      $new_opts['Rosa wichuraiana genome'][$k] = $v;
    }
    else {
      if ($k != 'SELECT_DB') {
        $new_opts['Other'][$k] = $v;
      }
    }
  }
  return remove_empty_opt_group ($new_opts);
}

/**
 * CottonGen specific blast db grouping
 *
 * @param unknown $options
 * @return unknown
 */
function organize_cottongen_blast_dbs ($options) {
  // sort options by weight
  $w_opts = array();
  foreach($options AS $k => $v) {
    $weight = 0;
    if (preg_match('/reftrans/i', $options[$k])) {
      $weight += 10000;
    }
    if (preg_match('/--/', $options[$k])) {
      $weight += 1000;
    }
    if (preg_match('/G\. a/', $options[$k])) {
      $weight += 100;
    } else if (preg_match('/G\. b/', $options[$k])) {
      $weight += 200;
    } else if (preg_match('/G\. h/', $options[$k])) {
      $weight += 300;
    } else if (preg_match('/G\. r/', $options[$k])) {
      $weight += 400;
    }
    if (preg_match('/BGI /', $options[$k])) {
      $weight += 10;
    } else if (preg_match('/JGI /', $options[$k])) {
      $weight += 20;
    } else if (preg_match('/HAU /', $options[$k])) {
      $weight += 30;
    } else if (preg_match('/NBI /', $options[$k])) {
      $weight += 40;
    } else if (preg_match('/NCGR /', $options[$k])) {
      $weight += 50;
    }
    if (preg_match('/ gene /', $options[$k])) {
      $weight += 1;
    } else if (preg_match('/ cDNA /', $options[$k])) {
      $weight += 2;
    } else if (preg_match('/ transcript /i', $options[$k])) {
      $weight += 3;
    } else if (preg_match('/ cds /i', $options[$k])) {
      $weight += 4;
    }
    $w_opts[$k] = $weight;
  }
  asort($w_opts);
  $sorted_opts = array();
  foreach($w_opts AS $k => $v) {
    $sorted_opts[$k] = $options[$k];
  }
  $new_opts =
  array(
    0 => 'SELECT a Dataset',
    'Peptide' => array(),
    'NCBI' => array(),
    'Marker' => array(),
    'Transcriptome' => array(),
    'Genome' => array(),
    'Other' => array()
  );
  foreach ($sorted_opts AS $k => $v) {
    if (preg_match('/proteins/i', $v) || preg_match('/genbank .+ nr/i', $v) || preg_match('/uniprotkb/i', $v)) {
      $new_opts['Peptide'][$k] = $v;
    }
    else if (preg_match('/genbank/i', $v) && !preg_match('/unigene/i', $v)) {
      $new_opts['NCBI'][$k] = $v;
    }
    else if ((preg_match('/cottongen/i', $v) || preg_match('/array/i', $v)) && !preg_match('/unigene/i', $v) && !preg_match('/reftrans/i', $v)) {
      $new_opts['Marker'][$k] = $v;
    }
    else if (preg_match('/RefTrans/i', $v)) {
      $new_opts['Transcriptome'][$k] = $v;
    }
    else if ((preg_match('/gene/i', $v) || preg_match('/cds/i', $v) || preg_match('/contig/i', $v) || preg_match('/cdna/i', $v)) && !preg_match('/genome/i', $v)) {
      $new_opts['Transcriptome'][$k] = $v;
    }
    else if (preg_match('/genome/i', $v) || preg_match('/chromosome/i', $v) || preg_match('/scaffold/i', $v)) {
      $new_opts['Genome'][$k] = $v;
    }
    else {
      if ($k != 'SELECT_DB') {
        $new_opts['Other'][$k] = $v;
      }
    }
  }
  //ksort($new_opts['Peptide']);
  ksort($new_opts['NCBI']);
  ksort($new_opts['Marker']);
  //ksort($new_opts['Transcriptome']);
  return remove_empty_opt_group ($new_opts);
}

/**
 * Citrus Genome DB specific blast db grouping
 *
 * @param unknown $options
 * @return unknown
 */
function organize_citrus_blast_dbs ($options) {
  $new_opts =
    array(
      0 => 'SELECT a Dataset',
      'Atalantia buxifolia genome' => array(),
      'C. changshanensis genome' => array(),
      'Citropsis gilletiana genome' => array(),
      'Citrus aurantifolia genome' => array(),
      'Citrus australasica genome' => array(),
      'Citrus australis genome' => array(),
      'Citrus clementina genome' => array(),
      'Citrus garrawayi genome' => array(),
      'Citrus glauca genome' => array(),
      'Citrus hongheensis genome' => array(),
      'Citrus ichangensis genome' => array(),
      'Citrus inodora genome' => array(),
      'Citrus lansium genome' => array(),
      'Citrus limon genome' => array(),
      'Citrus linwuensis genome' => array(),
      'Citrus mangshanensis genome' => array(),
      'Citrus maxima genome' => array(),
      'Citrus medica genome' => array(),
      'Citrus reticulata genome' => array(),
      'Citrus sinensis genome' => array(),
      'Fortunella hindsii genome' => array(),
      'Luvunga scandens genome' => array(),
      'Poncirus trifoliata genome' => array(),
      'Murraya paniculata genome' => array(),
      'RefTrans' => array(),
      'Unigenes' => array(),
      'ESTs' => array(),
      'Ca. Liberibacter asiaticus' => array(),
      'Other Ca. Liberibacter' => array(),
      'Liberibacter crescens' => array()
    );
  foreach ($options AS $k => $v) {
    if (preg_match('/Atalantia buxifolia/', $v)) {
      $new_opts['Atalantia buxifolia genome'][$k] = $v;
    }
    else if (preg_match('/C.+? changshanensis/', $v)) {
      $new_opts['C. changshanensis genome'][$k] = $v;
    }
    else if (preg_match('/Citropsis gilletiana/', $v)) {
      $new_opts['Citropsis gilletiana genome'][$k] = $v;
    }
    else if (preg_match('/C.+? aurantifolia/', $v)) {
      $new_opts['Citrus aurantifolia genome'][$k] = $v;
    }
    else if (preg_match('/C.+? australasica/', $v)) {
      $new_opts['Citrus australasica genome'][$k] = $v;
    }
    else if (preg_match('/C.+? australis/', $v)) {
      $new_opts['Citrus australis genome'][$k] = $v;
    }
    else if (preg_match('/C. clementina/', $v)) {
      $new_opts['Citrus clementina genome'][$k] = $v;
    }
    else if (preg_match('/C.+? garrawayi/', $v)) {
      $new_opts['Citrus glauca genome'][$k] = $v;
    }
    else if (preg_match('/C.+? glauca/', $v)) {
      $new_opts['Citrus glauca genome'][$k] = $v;
    }
    else if (preg_match('/C.+? hongheensis/', $v)) {
      $new_opts['Citrus hongheensis genome'][$k] = $v;
    }
    else if (preg_match('/C.+? ichangensis/', $v)) {
      $new_opts['Citrus ichangensis genome'][$k] = $v;
    }
    else if (preg_match('/C.+? inodora/', $v)) {
      $new_opts['Citrus inodora genome'][$k] = $v;
    }
    else if (preg_match('/C.+? lansium/', $v)) {
      $new_opts['Citrus lansium genome'][$k] = $v;
    }
    else if (preg_match('/C.+? limon/', $v)) {
      $new_opts['Citrus limon genome'][$k] = $v;
    }
    else if (preg_match('/C.+? linwuensis/', $v)) {
      $new_opts['Citrus linwuensis genome'][$k] = $v;
    }
    else if (preg_match('/C.+? mangshanensis/', $v)) {
      $new_opts['Citrus mangshanensis genome'][$k] = $v;
    }
    else if (preg_match('/C.+? maxima/', $v)) {
      $new_opts['Citrus maxima genome'][$k] = $v;
    }
    else if (preg_match('/C.+? medica/', $v)) {
      $new_opts['Citrus medica genome'][$k] = $v;
    }
    else if (preg_match('/C.+? reticulata/', $v)) {
      $new_opts['Citrus reticulata genome'][$k] = $v;
    }
    else if (preg_match('/C.+? sinensis/', $v)) {
      $new_opts['Citrus sinensis genome'][$k] = $v;
    }
    else if (preg_match('/Fortunella hindsii/', $v)) {
      $new_opts['Fortunella hindsii genome'][$k] = $v;
    }
    else if (preg_match('/Luvunga scandens/', $v)) {
      $new_opts['Luvunga scandens genome'][$k] = $v;
    }
    else if (preg_match('/Murraya paniculata/', $v)) {
      $new_opts['Murraya paniculata genome'][$k] = $v;
    }
    else if (preg_match('/RefTrans/', $v)) {
      $new_opts['RefTrans'][$k] = $v;
    }
    else if (preg_match('/unigene/', $v)) {
      $new_opts['Unigenes'][$k] = $v;
    }
    else if (preg_match('/ESTs/', $v)) {
      $new_opts['ESTs'][$k] = $v;
    }
    else if (preg_match('/Ca. L. ?asiaticus/', $v)) {
      $new_opts['Ca. Liberibacter asiaticus'][$k] = $v;
    }
    else if (preg_match('/Ca. L./', $v) || preg_match('/Liberibacter/', $v)) {
      $new_opts['Other Ca. Liberibacter'][$k] = $v;
    }
    else if (preg_match('/L. crescens/', $v)) {
      $new_opts['Liberibacter crescens'][$k] = $v;
    }
    else if (preg_match('/Poncirus trifoliata/', $v)) {
      $new_opts['Poncirus trifoliata genome'][$k] = $v;
    }
    else {
      if ($k != 'SELECT_DB' && $k != 0) {
        $new_opts['Other'][$k] = $v;
      }
    }
  }
  return remove_empty_opt_group ($new_opts);
}

/**
 * PCD specific blast db grouping
 *
 * @param unknown $options
 * @return unknown
 */
function organize_legume_blast_dbs ($options) {
  $new_opts =
  array(
    0 => 'SELECT a Dataset',
    'Adzuki Bean' => array(),
    'Chickpea' => array(),
    'Common Bean' => array(),
    'Cowpea' => array(),
    'Faba Bean' => array(),
    'Lentil' => array(),
    'Lima Bean' => array(),
    'Lupin' => array(),
    'Mung bean' => array(),
    'Rice bean' => array(),
    'Pea' => array(),
    'Pigeon Pea' => array(),    
    'Tepary Bean' => array(),
    'Vetch' => array(),
    'Vigna stipulacea' => array(),
    'Related Model Genomes' => array(),
  );
  foreach ($options AS $k => $v) {
    if (preg_match('/Vigna angularis/', $v)) {
      $new_opts['Adzuki Bean'][$k] = $v;
    }
    else if (preg_match('/Cicer /', $v)) {
      $new_opts['Chickpea'][$k] = $v;
    }
    else if (preg_match('/Phaseolus vulgaris/', $v)) {
      $new_opts['Common Bean'][$k] = $v;
    }
    else if (preg_match('/Vigna unguiculata/', $v)) {
      $new_opts['Cowpea'][$k] = $v;
    }
    else if (preg_match('/Vicia faba/', $v)) {
      $new_opts['Faba Bean'][$k] = $v;
    }
    else if (preg_match('/Vicia sativa|villosa/', $v)) {
      $new_opts['Vetch'][$k] = $v;
    }
    else if (preg_match('/Vigna stipulacea/', $v)) {
      $new_opts['Vigna stipulacea'][$k] = $v;
    }
    else if (preg_match('/Lens /', $v)) {
      $new_opts['Lentil'][$k] = $v;
    }
    else if (preg_match('/Phaseolus lunatus/', $v)) {
      $new_opts['Lima Bean'][$k] = $v;
    }
    else if (preg_match('/Lupinus /', $v)) {
      $new_opts['Lupin'][$k] = $v;
    }
    else if (preg_match('/Vigna radiata /', $v)) {
      $new_opts['Mung bean'][$k] = $v;
    }
    else if (preg_match('/Vigna umbellata /', $v)) {
      $new_opts['Rice bean'][$k] = $v;
    }
    else if (preg_match('/Pisum /', $v)) {
      $new_opts['Pea'][$k] = $v;
    }
    else if (preg_match('/Cajanus cajan/', $v)) {
      $new_opts['Pigeon Pea'][$k] = $v;
    }
    else if (preg_match('/Phaseolus acutifolius/', $v)) {
      $new_opts['Tepary Bean'][$k] = $v;
    }
    else {
      if ($k != 'SELECT_DB') {
        $new_opts['Related Model Genomes'][$k] = $v;
      }
    }
    // hide Related Model Genomes
    unset($new_opts['Related Model Genomes']);
  }
  return remove_empty_opt_group ($new_opts);
}

function remove_empty_opt_group ($new_opts) {
  $opts = array();
  foreach ($new_opts AS $k => $v) {
    if (is_array($v) && count($v) == 0) {
      continue;
    }
    else {
      $opts[$k] = $v;
    }
  }
  return $opts;
}
